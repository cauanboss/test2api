package main

import (
	"log"
	"net/http"
	"teste2api/presenter/api"
)

func handleRequest() {
	http.Handle("/", api.GetRouting())
	log.Fatal(http.ListenAndServe(":8101", nil))
}

func main() {
	handleRequest()
}
