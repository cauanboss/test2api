package mongodb

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var collection *mongo.Collection
var client *mongo.Client
var database *mongo.Database

func createClient() (*mongo.Client, error) {
	if client != nil {
		return client, nil
	}
	co := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), co)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	e := client.Ping(context.TODO(), nil)
	if e != nil {
		fmt.Println(e)
		return nil, e
	}
	return client, nil
}

func getDatabase() (*mongo.Database, error) {
	if database != nil {
		return database, nil
	}
	cli, err := createClient()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	database = cli.Database("test2")
	return database, nil
}

func GetCollection(cName string) *mongo.Collection {
	if collection != nil {
		return collection
	}
	db, err := getDatabase()
	if err != nil {
		panic(err)
	}
	collection := db.Collection(cName)
	return collection
}
