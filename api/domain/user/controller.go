package user

import (
	"context"
	"fmt"
	"log"
	"teste2api/domain/user/dto"
	"teste2api/domain/user/repository"

	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
}

func (uc UserController) FindOne() {
}

func (uc UserController) FindAll(userModel dto.FindAll) []dto.FindAll {
	ctx := context.TODO()
	users := []dto.FindAll{}
	cursor, err := repository.User().Find(ctx, bson.M{})
	if err != nil {
		panic(err)
	}

	if err = cursor.All(ctx, &users); err != nil {
		panic(err)
	}

	fmt.Println(users, userModel)
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}
	return users
}

func (uc UserController) Insert(body dto.InsertUser) (*mongo.InsertOneResult, error) {
	insert, err := repository.User().InsertOne(context.TODO(), &body)
	if err != nil {
		return nil, err
	}
	return insert, nil
}

func (uc UserController) DeleteOne() {

}
