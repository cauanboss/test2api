package repository

import (
	"teste2api/adapter/database/mongodb"

	"go.mongodb.org/mongo-driver/mongo"
)

func User() *mongo.Collection {
	return mongodb.GetCollection("user")
}
