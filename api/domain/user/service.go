package user

import (
	"encoding/json"
	"fmt"
	"net/http"
	"teste2api/domain/user/dto"

	"github.com/gorilla/mux"
)

type UserService struct {
}

func (us UserService) findOne(w http.ResponseWriter, r *http.Request) {
}

func (us UserService) findAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	user := dto.FindAll{}
	users := UserController{}.FindAll(user)
	json.NewEncoder(w).Encode(users)
}

func (us UserService) createUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	u := dto.InsertUser{}
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		fmt.Println("error", err)
		http.Error(w, err.Error(), 500)
		return
	}
	insert, err := UserController{}.Insert(u)
	if err != nil {
		fmt.Println("error", err)
		http.Error(w, err.Error(), 500)
		return
	}
	json.NewEncoder(w).Encode(insert)
}

func (us UserService) deleteOne(w http.ResponseWriter, r *http.Request) {

}

func (us UserService) Service(app *mux.Router) *mux.Router {
	app.HandleFunc("/{name}", us.findOne).Methods("GET")
	app.HandleFunc("", us.findAll).Methods("GET")
	app.HandleFunc("", us.createUser).Methods("POST")
	app.HandleFunc("/{id}", us.deleteOne).Methods("DELETE")
	return app
}
