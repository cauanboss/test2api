package api

import (
	"teste2api/domain/user"

	"github.com/gorilla/mux"
)

func GetRouting() *mux.Router {
	app := mux.NewRouter()

	app.HandleFunc("", nil).Methods("GET")
	appUser := app.PathPrefix("/user").Subrouter()
	user.UserService{}.Service(appUser)
	return app
}
